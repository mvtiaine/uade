#ifndef _UADE_WRITE_AUDIO_UTIL_H_
#define _UADE_WRITE_AUDIO_UTIL_H_

#include <cstdint>

namespace uade {
namespace write_audio {
namespace util {

int64_t get_ms_time();

}  // namespace util
}  // namespace write_audio
}  // namespace uade

#endif
