#ifndef _UADE_STRSEP_H_
#define _UADE_STRSEP_H_

char *strsep(char **stringp, const char *delim);

#endif
