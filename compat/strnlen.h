#ifndef _UADE_STRNLEN_H_
#define _UADE_STRNLEN_H_

#include <stddef.h>

size_t strnlen(const char *s, size_t len);

#endif
