#ifndef _UADE_STRCASESTR_H_
#define _UADE_STRCASESTR_H_

char *strcasestr(const char *s1, const char *s2);

#endif
